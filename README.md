# grig.synth

[![pipeline status](https://gitlab.com/haxe-grig/grig.synth/badges/master/pipeline.svg)](https://gitlab.com/haxe-grig/grig.synth/commits/master)
[![Build Status](https://travis-ci.org/osakared/grig.synth.svg?branch=master)](https://travis-ci.org/osakared/grig.synth)
[![Gitter](https://badges.gitter.im/haxe-grig/Lobby.svg)](https://gitter.im/haxe-grig/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

See the [haxe grig documentation](https://grig.tech/)

Individual modules to put together to make synths, with some modules being full-fledged synths in their own right.

Works more like a modular analog synth for maximum flexibility but with converters to work with the more rigid world of midi.

Also includes a port of [libfmsynth](https://github.com/Themaister/libfmsynth).
            
## License

This code is under the permissive MIT license, see [LICENSE](LICENSE).
